<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
    <head>
    </head>
    <body id="page-top" onLoad="inicio();">
        <header class="mastheadMini bg-primary text-white text-center mb-0" style='height: 10px;'>
            <div class="container-sm d-flex align-items-center flex-column" >
                <!-- Masthead Heading-->
                <!--h3 class="text-uppercase mt-0 mb-0"><i class="fas fa-list"></i> Operaciones</h3-->
            </div>
        </header>
        
        
        <!--section-->    
        <section id="Section_listadoOperaciones" class="page-section portfolio mt-2 mb-5">
            <div class="container">
                <div class="row mt-5 ml-5">
                    <h3 class="text-secondary col-md-6 col-sm-12 centerSm">INTEGRANTES</h3>
                </div>
                
                <hr>
                           
                <!--GRUPOS-->
                <div class="table-responsive-md mt-5" >
                    <table id="tabLstIntegrantes" class="table table-hover nowrap" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="hidden">Codigo</th>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Credito N°</th>
                                <th>Monto</th>
                                <th></th>
                            </tr>
                        </thead>
                         <tbody>
                            <tr>
                                <td class="hidden">1</td>
                                <td class="text-center ModalDetCred">Cristhian</td>
                                <td class="text-center ModalDetCred">Ocola</td>
                                <td class="text-center ModalDetCred">001-548248215</td>
                                <td class="text-center ModalDetCred">500</td>
                                <td><input type="text" class="input-sm"></td>
                                <td><button type="button" class="btn btnIcon" id="btnDetalle1" data-toggle="modal" data-target="#ModalDetCred"><i class="fas fa-info-circle fa-3x"></i></button></td>
                            </tr>
                            <tr>
                                <td class="hidden">1</td>
                                <td class="text-center ModalDetCred">Anthony</td>
                                <td class="text-center ModalDetCred">Benavides</td>
                                <td class="text-center ModalDetCred">001-985478524</td>
                                <td class="text-center ModalDetCred">300</td>
                                <td><input type="text" class="input-sm"></td>
                                <td><button type="button" class="btn btnIcon" id="btnDetalle1" data-toggle="modal" data-target="#ModalDetCred"><i class="fas fa-info-circle fa-3x"></i></button></td>
                        </tbody> 
                    </table>
                </div>
                
                
                
                <div class="mt-5 offset-lg-8 mb-5 " >
                    <h3>MONTO TOTAL: 800</h3>
                </div>
                
                <div class="row offset-lg-7 offset-md-5 justify-content-center mt-4 mb-3">
                    <button id="btnGrabar" type="button" class="btn notCenter btnIconSuccess mr-5 btnGrabar" title="Desembolsar"  onclick="flujo_grabar();">
                        Desembolsar
                    </button>

                    <button type="button" class="btn notCenter btnIconDanger" title="Pagar" onclick="flujo_cancelar();">
                         Pagar
                    </button>
                </div>
                
                
                <div class="portfolio-modal modal fade" id="ModalDetCred" tabindex="-1" role="dialog" aria-labelledby="ModalDetCred1Label" aria-hidden="true">
                    <div class="modal-dialog modal-lg " role="document">
                        <div class="modal-content">
                            <button class="close btn btnIconPrimary" type="button" data-dismiss="modal" aria-label="Close">
                                <i class="fas fa-times fa-xs"></i>
                            </button>
                            <h2 class="text-secondary mb-0 mt-5">DETALLE CREDITO N°<span id="ModalCodCred"></span></h2>
                            <hr>
                            <div class="row">
                                <div class="col-lg-2 col-sm-4 ml-auto">
                                    <p class="lead">Titular:</p>
                                </div>
                                <div class="col-lg-3 col-sm-8">
                                    <p id="ModalTitular" class="leadBigger"></p>
                                </div>
                                <div class="col-lg-2 col-sm-4">
                                    <p class="lead">N° Socio:</p>
                                </div>
                                <div class="col-lg-3 col-sm-8 mr-auto">
                                    <p id="ModalNSocio" class="leadBigger"></p>
                                </div>
                            </div>

                            <p class="leadBigger mb-0 mt-5 ml-5">Detalles del Credito:</p>
                            <hr/>

                            <div class="row">
                                <div class="col-lg-2 col-sm-4  ml-auto">
                                    <p class="lead">Credito N°:</p>
                                </div>
                                <div class="col-lg-3 col-sm-8">
                                    <p id="ModalNCredito" class="leadBigger"></p>
                                </div>
                                <div class="col-lg-2 col-sm-4">
                                    <p class="lead">Monto:</p>
                                </div>
                                <div class="col-lg-3 col-sm-8 mr-auto">
                                    <p id="ModalMonto" class="leadBigger"></p>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-lg-2 col-sm-4 ml-auto">
                                    <p class="lead">Interes:</p>
                                </div>
                                <div class="col-lg-3 col-sm-8">
                                    <p id="ModalInteres" class="leadBigger"></p>
                                </div>

                                <div class="col-lg-2 col-sm-4">
                                    <p class="lead">Mora:</p>
                                </div>
                                <div class="col-lg-3 col-sm-8 mr-auto">
                                    <p id="ModalMora" class="leadBigger"></p>
                                </div>
                            </div>
                            
                            <div class="row mt-5 mb-5">
                                <div class="col-lg-2 col-sm-4 ml-auto">
                                    <p class="lead">Gastos:</p>
                                </div>
                                <div class="col-lg-3 col-sm-8">
                                    <p id="ModalGastos" class="leadBigger"></p>
                                </div>
                                <div class="col-lg-2 col-sm-4">
                                    <p class="lead">Otros:</p>
                                </div>
                                <div class="col-lg-3 col-sm-8 mr-auto">
                                    <p id="ModalOtros" class="leadBigger"></p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>     
               
    </body>
</html>
