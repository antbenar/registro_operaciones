<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <script src="js/jsPages/Grupo.js"></script>
    </head>
    <body id="page-top" onLoad="inicio();">
        <header class="mastheadMini bg-primary text-white text-center mb-0" style='height: 10px;'>
            <div class="container-sm d-flex align-items-center flex-column" >
                <!-- Masthead Heading-->
                <!--h3 class="text-uppercase mt-0 mb-0"><i class="fas fa-list"></i> Operaciones</h3-->
            </div>
        </header>
        
        
        <!--section-->    
        <section id="Section_listadoOperaciones" class="page-section portfolio mt-2 mb-5">
            <div class="container">
                <div class="row">
                    <h3 class="text-secondary col-md-6 col-sm-12 centerSm">LISTADO DE GRUPOS</h3>
                </div>
                
                <hr>
                
                
                <div class="row formBuscar"> 
                    <div class="col-md-2 ml-auto mr-5">
                        <h6>Nombre Grupo: </h6>
                        <input type="text" class="form-control text-uppercase input-sm text-center" placeholder="Nro documento cliente" id="inpNomGrupo"/>
                    </div>
                    <div class="col-md-2 mr-5">
                        <h6>Monto: </h6>
                        <input type="number" class="form-control text-uppercase input-sm text-center" placeholder="Monto" id="inpMonto"/>
                    </div>
                    <div class="col-md-1 mr-auto">
                        <button id="btnBuscar" type="button" class="btn btnIconInfo float-right mt-2" title="Buscar" onclick="fxFiltrarOperaciones();">
                            <i class="fas fa-search"></i> Buscar
                        </button>
                    </div>
                    
                </div> 
                
                <div class="table-responsive-md mt-5" >
                    <table id="tabLstGrupos" class="table table-hover nowrap" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="hidden">Codigo</th>
                                <th>Nombre</th>
                                <th>Integrantes</th>
                                <th>Monto</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="hidden">1</td>
                                <td class="text-center">las estrellitas</td>
                                <td class="text-center">3</td>
                                <td class="text-center">800</td>
                                <td class="text-center"><button type="button" class="btn btnIconSuccess float-right" title="Seleccionar" onclick="Integrantes()" >
                                        <i class="fas fa-check"></i> Seleccionar
                                    </button></td>
                            </tr>
                            <tr>
                                <td class="hidden">2s</td>
                                <td class="text-center">las estrellitas2</td>
                                <td class="text-center">4</td>
                                <td class="text-center">15200</td>
                                <td class="text-center"><button type="button" class="btn btnIconSuccess float-right" title="Seleccionar" onclick="Integrantes()" >
                                        <i class="fas fa-check"></i> Seleccionar
                                    </button></td>
                            </tr> 
                        </tbody>
                    </table>
                </div>
                
                
            </div>
        </section>     
               
    </body>
</html>
