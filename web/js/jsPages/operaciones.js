function inicio(){
    //inicializar el listado de operaciones
    fxFiltrarOperaciones();
    
    //iniciar con la pestania listado
    cambiarPestania("lst");     
    
    //cargar Selects
    fxcargarTipo();
    fxcargarOficina();  
    fxcargarTipoDocumento();
    fxcargarTipoRelacionSO();
    fxcargarCondResidencia();
    fxcargarTipoPersona();
    
    fxcargarAlcanceOperacion();
    fxcargarIntermediarioOperacion();
    fxcargarMoneda();
    fxcargarDestinoTipoCuentaInvolucrada();
    fxcargarOrigenTipoCuentaInvolucrada();
    fxcargarTipoFondos();
    fxcargarTipoOperacion();
    fxcargarFormaOperacion();
    fxcargarCodigoPaises();
    fxcargarDepartamentos();
    
    //Funciones misma persona para diferentes tipos de personas
    $("#FRO_checkCNRO").change(function() {
        if(this.checked) {
            copiarPersona("CNRO", "FRO");
            toastr.info("Datos copiados de CNRO a FRO");
        }
    });
    $("#FRO_checkFD").change(function() {
        if(this.checked) {
            copiarPersona("FD", "FRO");
            toastr.info("Datos copiados de FD a FRO");
        }
    });
    $("#CNRO_checkFRO").change(function() {
        if(this.checked) {
           copiarPersona("FRO", "CNRO");
           toastr.info("Datos copiados de FRO a CNRO");
        }
    });
    $("#CNRO_checkFD").change(function() {
        if(this.checked) {
            copiarPersona("FD", "CNRO");
            toastr.info("Datos copiados de FD a CNRO");
        }
    });
    $("#FD_checkFRO").change(function() {
        if(this.checked) {
            copiarPersona("FRO", "FD");
            toastr.info("Datos copiados de FRO a FD");
        }
    });
    $("#FD_checkCNRO").change(function() {
        if(this.checked) {
            copiarPersona("CNRO", "FD");
            toastr.info("Datos copiados de CNRO a FD");
        }
    });
    
    //rellenar datos de persona FRO
    $("#formFRO input[id='numeroDocumento']").on('keypress', function (e) {
            if(e.which === 13){
               $(this).attr("disabled", "disabled");
               var numDocumento = $("#formFRO input[id='numeroDocumento']").val();
               aplicarPersona(numDocumento, "FRO");
               
               $(this).removeAttr("disabled");
            }
      });
     //rellenar datos de persona CNRO
    $("#formCNRO input[id='numeroDocumento']").on('keypress', function (e) {
            if(e.which === 13){
               $(this).attr("disabled", "disabled");
               var numDocumento = $("#formCNRO input[id='numeroDocumento']").val();
               
               aplicarPersona(numDocumento, "CNRO");
               $(this).removeAttr("disabled");
            }
      });
     //rellenar datos de persona FD
    $("#formFD input[id='numeroDocumento']").on('keypress', function (e) {
            if(e.which === 13){
               $(this).attr("disabled", "disabled");
               var numDocumento = $("#formFD input[id='numeroDocumento']").val();
               aplicarPersona(numDocumento, "FD");
               $(this).removeAttr("disabled");
            }
      });
      
}

//cambiar entre mantenimiento y listado
function cambiarPestania(nomSeccion) {
    if (nomSeccion === "lst") {
        $('#Section_listadoOperaciones').show();
        $('#Section_nuevaOperacion').hide();
    } else if (nomSeccion === "mnt") {
        $('#Section_nuevaOperacion').show();
        $('#Section_listadoOperaciones').hide();
        $("#documento").focus();
    }
}

function cambiarPestaniaTipo(num) {
    $('#liPas1').removeClass('active');
    $('#liPas2').removeClass('active');
    $('#liPas3').removeClass('active');
    
    if (num === "FRO") {
           if (!$('#liPas1').hasClass('active')) {
                $('#formFRO').show();
                $('#formCNRO').hide();
                $('#formFD').hide();
                $('#liPas1').addClass('active');
                $("#formFRO input[id='numeroDocumento']").focus();
           }
    } else if (num === "CNRO") {
           if (!$('#liPas2').hasClass('active')) {
               $('#formFRO').hide();
                $('#formCNRO').show();
                $('#formFD').hide();
                $('#liPas2').addClass('active');
                $("#formCNRO input[id='numeroDocumento']").focus();
            }
    } else if (num === "FD") {
           if (!$('#liPas3').hasClass('active')) {
               $('#formFRO').hide();
                $('#formCNRO').hide();
                $('#formFD').show();
                $('#liPas3').addClass('active');
                $("#formFD input[id='numeroDocumento']").focus();
            }
    }
    
    
    //remover el atributo de seleccionado de los checkbox
    $(".form-check-input").each(function(){
        $(this).prop('checked', false); 
    });
}

//===============================================================================================
//
//                            funciones cargar selects
//                            
//==============================================================================================

function fxcargarTipo() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
    
    $.ajax({
        type: 'POST',
        url: 'listarTipos.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#tipo').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarOficina() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
        $.ajax({
        type: 'POST',
        url: 'listarOficinas.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#oficina').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}


function fxcargarTipoDocumento() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
 
    $.ajax({
        type: 'POST',
        url: 'listarTipoDocumento.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#tipoDocCliente').html(htmlData);
                
                $("#formFRO select[id='tipoDocumento']").html(htmlData);
                $("#formCNRO select[id='tipoDocumento']").html(htmlData);
                $("#formFD select[id='tipoDocumento']").html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarTipoRelacionSO() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
    
    $.ajax({
        type: 'POST',
        url: 'listarTipoRelacionSO.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                    $("#formFRO select[id='tipoRelacionSO']").html(htmlData);
                    $("#formCNRO select[id='tipoRelacionSO']").html(htmlData);
                    $("#formFD select[id='tipoRelacionSO']").html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarCondResidencia() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
    
    $.ajax({
        type: 'POST',
        url: 'listarCondResidencia.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $("#formFRO select[id='condResidencia']").html(htmlData);
                $("#formCNRO select[id='condResidencia']").html(htmlData);
                $("#formFD select[id='condResidencia']").html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarTipoPersona() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
    
    $.ajax({
        type: 'POST',
        url: 'listarTipoPersona.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $("#formFRO select[id='tipoPersona']").html(htmlData);
                $("#formCNRO select[id='tipoPersona']").html(htmlData);
                $("#formFD select[id='tipoPersona']").html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarTipoFondos() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
    
    $.ajax({
        type: 'POST',
        url: 'listarTipoFondos.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#tipoFondos').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarTipoOperacion() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
    
    $.ajax({
        type: 'POST',
        url: 'listarTipoOperacion.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#tipoOperacion').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarMoneda() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
        $.ajax({
        type: 'POST',
        url: 'listarMoneda.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#moneda').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarAlcanceOperacion() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
    $.ajax({
        type: 'POST',
        url: 'listarAlcanceOperacion.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#alcanceOperacion').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarIntermediarioOperacion() {
    var htmlData = '<option value="-1">--Seleccione--</option>';

    $.ajax({
        type: 'POST',
        url: 'listarTipos.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#intermediarioOperacion').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}


function fxcargarFormaOperacion() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
    $.ajax({
        type: 'POST',
        url: 'listarFormaOperacion.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#formaOperacion').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarOrigenTipoCuentaInvolucrada() {
    var htmlData = '<option value="-1">--Seleccione--</option>';

    $.ajax({
        type: 'POST',
        url: 'listarOrigenTipoCuentaInvolucrada.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#origenTipoCuentaInvolucrada').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}


function fxcargarDestinoTipoCuentaInvolucrada() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
    $.ajax({
        type: 'POST',
        url: 'listarDestinoTipoCuentaInvolucrada.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#destinoTipoCuentaInvolucrada').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}


function fxcargarCodigoPaises() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
 
    $.ajax({
        type: 'POST',
        url: 'listarCodigoPaises.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $('#codPaisOrigen').html(htmlData);
                $('#codPaisDestino').html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

function fxcargarDepartamentos() {
    var htmlData = '<option value="-1">--Seleccione--</option>';
        
    $.ajax({
        type: 'POST',
        url: 'listarDepartamentos.action',
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                //SELECTS ANIDADOS
                //FRO
                $("#formFRO select[id='departamento']").html(htmlData);
                $("#formFRO select[id='departamento']").change(function() {
                    $("#formFRO select[id='distrito']").html('<option value="-1">--Seleccione--</option>');
                    fxcargarProvincias('FRO');
                });

                //CNRO
                $("#formCNRO select[id='departamento']").html(htmlData);
                $("#formCNRO select[id='departamento']").change(function() {
                    $("#formCNRO select[id='distrito']").html('<option value="-1">--Seleccione--</option>');
                    fxcargarProvincias('CNRO');
                });

                //FD
                $("#formFD select[id='departamento']").html(htmlData);
                $("#formFD select[id='departamento']").change(function() {
                    $("#formFD select[id='distrito']").html('<option value="-1">--Seleccione--</option>');
                    fxcargarProvincias('FD');
                });
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}


function fxcargarProvincias(tipoPersona){
    //obtener el codigo del departamento
    var codigoDepartamento = $("#form"+tipoPersona+" select[id='departamento']").val();
    var parametros = {
        codigo: codigoDepartamento
    };
    var htmlData = '<option value="-1">--Seleccione--</option>';
    
    $.ajax({
        type: 'POST',
        url: 'listarProvincias.action',
        data: parametros,
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                //SELECTS ANIDADOS
                $("#form"+tipoPersona+" select[id='provincia']").html(htmlData);
                $("#form"+tipoPersona+" select[id='provincia']").change(function() {
                    fxcargarDistritos(tipoPersona);
                });
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });

}




function fxcargarDistritos(tipoPersona){
    //obtener el codigo del departamento
    var codigoProvincia = $("#form"+tipoPersona+" select[id='provincia']").val();
    var parametros = {
        codigo: codigoProvincia
    };
    
    var htmlData = '<option value="-1">--Seleccione--</option>';
    
    $.ajax({
        type: 'POST',
        url: 'listarDistritos.action',
        data: parametros,
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                $("#form"+tipoPersona+" select[id='distrito']").html(htmlData);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}

//===============================================================================================
//
//                            funciones aplicar persona
//                            
//==============================================================================================


function fxAplicarProvincia(tipoPersona, codProvincia, codDistrito){
    
       //obtener el codigo del departamento
    var codigoDepartamento = $("#form"+tipoPersona+" select[id='departamento']").val();
    var parametros = {
        codigo: codigoDepartamento
    };
    var htmlData = '<option value="-1">--Seleccione--</option>';
    
    $.ajax({
        type: 'POST',
        url: 'listarProvincias.action',
        data: parametros,
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                //SELECTS ANIDADOS
                $("#form"+tipoPersona+" select[id='provincia']").html(htmlData);
                $("#form"+tipoPersona+" select[id='provincia']").val(codProvincia);
                
                $("#form"+tipoPersona+" select[id='provincia']").change(function() {
                    fxcargarDistritos(tipoPersona);
                });
                
                fxAplicarDistrito(tipoPersona, codDistrito);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}


function fxAplicarDistrito(tipoPersona, codDistrito){
    
       //obtener el codigo del departamento
    var codigoProvincia = $("#form"+tipoPersona+" select[id='provincia']").val();
    var parametros = {
        codigo: codigoProvincia
    };
    var htmlData = '<option value="-1">--Seleccione--</option>';
    
    $.ajax({
        type: 'POST',
        url: 'listarDistritos.action',
        data: parametros,
        success: function (data) {
            if (data.err.length === 0) {
                $.each(data.listTabla, function () {
                    htmlData += '<option value="' + this.codigo + '">' + this.nombre + '</option>';
                });
                //SELECTS ANIDADOS
                $("#form"+tipoPersona+" select[id='distrito']").html(htmlData);
                $("#form"+tipoPersona+" select[id='distrito']").val(codDistrito);
                
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + errorThrown);
        }
    });
}



//Funcion que copia los datos de un tipo de persona a otro tipo de persona
function copiarPersona(tipoPersona1, tipoPersona2){
    $("#form"+tipoPersona2+" select[id='tipoRelacionSO']").val($("#form"+tipoPersona1+" select[id='tipoRelacionSO']").val());
    $("#form"+tipoPersona2+" select[id='condResidencia']").val($("#form"+tipoPersona1+" select[id='condResidencia']").val());
    $("#form"+tipoPersona2+" select[id='tipoPersona']").val($("#form"+tipoPersona1+" select[id='tipoPersona']").val());
    $("#form"+tipoPersona2+" select[id='tipoDocumento']").val($("#form"+tipoPersona1+" select[id='tipoDocumento']").val());
    $("#form"+tipoPersona2+" input[id='numeroDocumento']").val($("#form"+tipoPersona1+" input[id='numeroDocumento']").val());
    $("#form"+tipoPersona2+" input[id='numeroRuc']").val($("#form"+tipoPersona1+" input[id='numeroRuc']").val());
    $("#form"+tipoPersona2+" input[id='apellidoPaterno']").val($("#form"+tipoPersona1+" input[id='apellidoPaterno']").val());
    $("#form"+tipoPersona2+" input[id='apellidoMaterno']").val($("#form"+tipoPersona1+" input[id='apellidoMaterno']").val());
    $("#form"+tipoPersona2+" input[id='nombres']").val($("#form"+tipoPersona1+" input[id='nombres']").val());
    $("#form"+tipoPersona2+" input[id='ocupacion']").val($("#form"+tipoPersona1+" input[id='ocupacion']").val());
    $("#form"+tipoPersona2+" input[id='descOcupacion']").val($("#form"+tipoPersona1+" input[id='descOcupacion']").val());
    $("#form"+tipoPersona2+" input[id='CIIU']").val($("#form"+tipoPersona1+" input[id='CIIU']").val());
    $("#form"+tipoPersona2+" input[id='cargo']").val($("#form"+tipoPersona1+" input[id='cargo']").val());
    $("#form"+tipoPersona2+" input[id='telefono']").val($("#form"+tipoPersona1+" input[id='telefono']").val());
    $("#form"+tipoPersona2+" input[id='nombreVia']").val($("#form"+tipoPersona1+" input[id='nombreVia']").val());
    $("#form"+tipoPersona2+" select[id='departamento']").val($("#form"+tipoPersona1+" select[id='departamento']").val());
    var codProvincia = $("#form"+tipoPersona1+" select[id='provincia']").val();
    var codDistrito = $("#form"+tipoPersona1+" select[id='distrito']").val();
    fxAplicarProvincia(tipoPersona2,codProvincia, codDistrito);
}

function aplicarTipoPersona(persona, tipoPersona){  
    $("#form"+tipoPersona+" select[id='tipoRelacionSO']").val(persona.tipoRelacionSO);
    $("#form"+tipoPersona+" select[id='condResidencia']").val(persona.condResidencia);
    $("#form"+tipoPersona+" select[id='tipoPersona']").val(persona.tipoPersona);
    $("#form"+tipoPersona+" select[id='tipoDocumento']").val(persona.tipoDocumento);
    $("#form"+tipoPersona+" input[id='numeroDocumento']").val(persona.numeroDocumento);
    $("#form"+tipoPersona+" input[id='numeroRuc']").val(persona.numeroRuc);
    $("#form"+tipoPersona+" input[id='apellidoPaterno']").val(persona.apellidoPaterno);
    $("#form"+tipoPersona+" input[id='apellidoMaterno']").val(persona.apellidoMaterno);
    $("#form"+tipoPersona+" input[id='nombres']").val(persona.nombres);
    $("#form"+tipoPersona+" input[id='ocupacion']").val(persona.ocupacion);
    $("#form"+tipoPersona+" input[id='descOcupacion']").val(persona.descOcupacion);
    $("#form"+tipoPersona+" input[id='CIIU']").val(persona.ciiu);
    $("#form"+tipoPersona+" input[id='cargo']").val(persona.cargo);
    $("#form"+tipoPersona+" input[id='telefono']").val(persona.telefono);
    $("#form"+tipoPersona+" input[id='nombreVia']").val(persona.nombreVia);
    $("#form"+tipoPersona+" select[id='departamento']").val(persona.departamento);
    fxAplicarProvincia(tipoPersona,persona.provincia, persona.distrito);
}


function borrarDatosPersona(tipoPersona){
    $("#form"+tipoPersona+" select[id='tipoRelacionSO']").val("-1");
    $("#form"+tipoPersona+" select[id='condResidencia']").val("-1");
    $("#form"+tipoPersona+" select[id='tipoPersona']").val("-1");
    //$("#form"+tipoPersona+" select[id='tipoDocumento']").val("");
    //$("#form"+tipoPersona+" input[id='numeroDocumento']").val("");
    $("#form"+tipoPersona+" input[id='numeroRuc']").val("");
    $("#form"+tipoPersona+" input[id='apellidoPaterno']").val("");
    $("#form"+tipoPersona+" input[id='apellidoMaterno']").val("");
    $("#form"+tipoPersona+" input[id='nombres']").val("");
    $("#form"+tipoPersona+" input[id='ocupacion']").val("");
    $("#form"+tipoPersona+" input[id='descOcupacion']").val("");
    $("#form"+tipoPersona+" input[id='CIIU']").val("");
    $("#form"+tipoPersona+" input[id='cargo']").val("");
    $("#form"+tipoPersona+" input[id='telefono']").val("");
    $("#form"+tipoPersona+" input[id='nombreVia']").val("");
    $("#form"+tipoPersona+" select[id='departamento']").val("-1");
    $("#form"+tipoPersona+" select[id='provincia']").val("-1");
    $("#form"+tipoPersona+" select[id='distrito']").val("-1");
}

function  fxAplicarPersona(numDocumento, tipoPersona){
    var parametros = {
        nroDocumento: numDocumento
    };
    
    $.ajax({
        url: 'aplicarPersona.action',
        data: parametros,
        type: "POST",
        cache : false,
        async: false,
        dataType: "json",
        success: function(data) {
            console.log(data);
            if (data.err.length === 0) {
                if(data.persona.numeroDocumento === null){
                    toastr.info("Persona no se encuentra registrada");
                    borrarDatosPersona(tipoPersona);
                }
                else{
                    aplicarTipoPersona(data.persona, tipoPersona);
                    
                    toastr.info("Datos de " + data.persona.nombres + " " + data.persona.apellidoPaterno + " rellenados");
                }
            } else {
                toastr.error(data.err);
            }
        },
        error: function(jqXmlHttpRequest, textStatus, errorThrown) { toastr.error("Error: " + errorThrown); }
    }).done(function () {});
}

function  aplicarPersona(numDocumento, tipoPersona){  
    //verificar si el documento ingresado se encuentra ya en otro tipo de persona
    var docFRO =  $("#formFRO input[id='numeroDocumento']").val();
    var docCNRO =  $("#formCNRO input[id='numeroDocumento']").val();
    var docFD =  $("#formFD input[id='numeroDocumento']").val();
    
    if(tipoPersona !== "FRO" && numDocumento=== docFRO){
        copiarPersona("FRO", tipoPersona);
        toastr.info("El documento coincide con el documento de la persona FRO, los datos fueron copiados");
    }
    else if(tipoPersona !== "CNRO" && numDocumento=== docCNRO){
        copiarPersona("CNRO", tipoPersona);
        toastr.info("El documento coincide con el documento de la persona CNRO, los datos fueron copiados");
    }
    else if(tipoPersona !== "FD" && numDocumento=== docFD){
        copiarPersona("FD", tipoPersona);
        toastr.info("El documento coincide con el documento de la persona FD, los datos fueron copiados");
    }
    else{
        //consultar en la bd si esa persona existe
        fxAplicarPersona(numDocumento, tipoPersona);
    }
}



//===============================================================================================
//
//                            funciones aplicar operacion
//                            
//==============================================================================================


function aplicarOperacion(op){
    //DATOS DE LA OPERACIÓN    
    $("#formOperacion input[id='fecha']").val(op.fecha);
    $("#formOperacion input[id='hora']").val((op.hora).substring(0,5));
    $("#formOperacion input[id='documento']").val(op.documento);
    $("#formOperacion select[id='tipo']").val(op.tipo);
    $("#formOperacion select[id='oficina']").val(op.oficina);
    $("#formOperacion select[id='tipoDocCliente']").val(op.tipoDocCliente);
    $("#formOperacion input[id='nroDocCliente']").val(op.nroDocCliente);
    $("#formOperacion input[id='cliente']").val(op.nombreCliente);
    $("#formOperacion input[id='cuenta']").val(op.cuenta);
    $("#formOperacion input[id='monto']").val(op.monto);

    //DETALLES DE LA OPERACIÓN    
    $("#formDetOperacion select[id='tipoFondos']").val(op.tipoFondos);
    $("#formDetOperacion select[id='tipoOperacion']").val(op.tipoOperacion);
    $("#formDetOperacion input[id='descTipoOperacion']").val(op.descTipoOperacion);
    $("#formDetOperacion textarea[id='origenFondos']").val(op.origenFondos);
    $("#formDetOperacion select[id='moneda']").val(op.moneda);
    $("#formDetOperacion select[id='alcanceOperacion']").val(op.alcanceOperacion);
    $("#formDetOperacion select[id='codPaisOrigen']").val(op.codPaisOrigen);
    $("#formDetOperacion select[id='codPaisDestino']").val(op.codPaisDestino);
    $("#formDetOperacion select[id='intermediarioOperacion']").val(op.intermediarioOperacion);
    $("#formDetOperacion select[id='formaOperacion']").val(op.formaOperacion);
    $("#formDetOperacion input[id='descFormaOperacion']").val(op.descFormaOperacion);
    $("#formDetOperacion input[id='origenEntidadInvolucrada']").val(op.origenEntidadInvolucrada);
    $("#formDetOperacion select[id='origenTipoCuentaInvolucrada']").val(op.origenTipoCuentaInvolucrada);
    $("#formDetOperacion input[id='origenCodCuenta']").val(op.origenCodCuenta);
    $("#formDetOperacion input[id='destinoEntidadInvolucrada']").val(op.destinoEntidadInvolucrada);
    $("#formDetOperacion select[id='destinoTipoCuentaInvolucrada']").val(op.destinoTipoCuentaInvolucrada);
    $("#formDetOperacion input[id='destinoCodCuenta']").val(op.destinoCodCuenta);        

    //FRO - FISICAMENTE REALIZA LA OPERACION        
    $("#formFRO select[id='tipoRelacionSO']").val(op.fro.tipoRelacionSO);
    $("#formFRO select[id='condResidencia']").val(op.fro.condResidencia);
    $("#formFRO select[id='tipoPersona']").val(op.fro.tipoPersona);
    $("#formFRO select[id='tipoDocumento']").val(op.fro.tipoDocumento);
    $("#formFRO input[id='numeroDocumento']").val(op.fro.numeroDocumento);
    $("#formFRO input[id='numeroRuc']").val(op.fro.numeroRuc);
    $("#formFRO input[id='apellidoPaterno']").val(op.fro.apellidoPaterno);
    $("#formFRO input[id='apellidoMaterno']").val(op.fro.apellidoMaterno);
    $("#formFRO input[id='nombres']").val(op.fro.nombres);
    $("#formFRO input[id='ocupacion']").val(op.fro.ocupacion);
    $("#formFRO input[id='descOcupacion']").val(op.fro.descOcupacion);
    $("#formFRO input[id='CIIU']").val(op.fro.ciiu);
    $("#formFRO input[id='cargo']").val(op.fro.cargo);
    $("#formFRO input[id='telefono']").val(op.fro.telefono);
    $("#formFRO input[id='nombreVia']").val(op.fro.nombreVia);
    $("#formFRO select[id='departamento']").val(op.fro.departamento);
    fxAplicarProvincia("FRO",op.fro.provincia, op.fro.distrito);

    //CNRO - EN CUYO NOMBRE SE REALIZA LA OPERACION           
    $("#formCNRO select[id='tipoRelacionSO']").val(op.cnro.tipoRelacionSO);
    $("#formCNRO select[id='condResidencia']").val(op.cnro.condResidencia);
    $("#formCNRO select[id='tipoPersona']").val(op.cnro.tipoPersona);
    $("#formCNRO select[id='tipoDocumento']").val(op.cnro.tipoDocumento);
    $("#formCNRO input[id='numeroDocumento']").val(op.cnro.numeroDocumento);
    $("#formCNRO input[id='numeroRuc']").val(op.cnro.numeroRuc);
    $("#formCNRO input[id='apellidoPaterno']").val(op.cnro.apellidoPaterno);
    $("#formCNRO input[id='apellidoMaterno']").val(op.cnro.apellidoMaterno);
    $("#formCNRO input[id='nombres']").val(op.cnro.nombres);
    $("#formCNRO input[id='ocupacion']").val(op.cnro.ocupacion);
    $("#formCNRO input[id='descOcupacion']").val(op.cnro.descOcupacion);
    $("#formCNRO input[id='CIIU']").val(op.cnro.ciiu);
    $("#formCNRO input[id='cargo']").val(op.cnro.cargo);
    $("#formCNRO input[id='telefono']").val(op.cnro.telefono);
    $("#formCNRO input[id='nombreVia']").val(op.cnro.nombreVia);
    $("#formCNRO select[id='departamento']").val(op.cnro.departamento);
    fxAplicarProvincia("CNRO",op.cnro.provincia, op.cnro.distrito);
    
    //FD - A FAVOR DE   
    $("#formFD select[id='tipoRelacionSO']").val(op.fd.tipoRelacionSO);
    $("#formFD select[id='condResidencia']").val(op.fd.condResidencia);
    $("#formFD select[id='tipoPersona']").val(op.fd.tipoPersona);
    $("#formFD select[id='tipoDocumento']").val(op.fd.tipoDocumento);
    $("#formFD input[id='numeroDocumento']").val(op.fd.numeroDocumento);
    $("#formFD input[id='numeroRuc']").val(op.fd.numeroRuc);
    $("#formFD input[id='apellidoPaterno']").val(op.fd.apellidoPaterno);
    $("#formFD input[id='apellidoMaterno']").val(op.fd.apellidoMaterno);
    $("#formFD input[id='nombres']").val(op.fd.nombres);
    $("#formFD input[id='ocupacion']").val(op.fd.ocupacion);
    $("#formFD input[id='descOcupacion']").val(op.fd.descOcupacion);
    $("#formFD input[id='CIIU']").val(op.fd.ciiu);
    $("#formFD input[id='cargo']").val(op.fd.cargo);
    $("#formFD input[id='telefono']").val(op.fd.telefono);
    $("#formFD input[id='nombreVia']").val(op.fd.nombreVia);
    $("#formFD select[id='departamento']").val(op.fd.departamento);
    fxAplicarProvincia("FD",op.fd.provincia, op.fd.distrito);
}

function  fxAplicarOperacion(codigo){
    var parametros = {
        codigo: codigo
    };
    
    $.ajax({
        url: 'aplicarOperacion.action',
        data: parametros,
        type: "POST",
        cache : false,
        async: false,
        dataType: "json",
        success: function(data) { 
            if (data.err.length === 0) {
                aplicarOperacion(data.operacion);
                
                //preparar vista de mantenimiento
                cambiarPestaniaTipo("FRO");
                $("#btnGrabar").hide();
                cambiarPestania('mnt');
            } else {
                toastr.error(data.err);
            }
        },
        error: function(jqXmlHttpRequest, textStatus, errorThrown) { toastr.error("Error: " + errorThrown); }
    }).done(function () {});
}


//===============================================================================================
//
//                            funnciones listado operaciones
//                            
//==============================================================================================

function generarTablaOperaciones(datos){

    $.each(datos.lstOperaciones, function () {
        this.ver = '<button type="button" class="btn btnIcon float-center" title="Ver detalle"  onclick="fxAplicarOperacion(\''+ this.codigo +'\');"><i class="fas fa-eye fa-2x"></i></button>';
        this.reemitir = '<button type="button" class="btn btnIcon float-center" title="Reemitir"  onclick="fxReemitir(\''+ this.codigo +'\');"><i class="fas fa-print fa-2x"></i></button>';
    });
    
    console.log(datos);
    $('#tabLstOperaciones').DataTable({
        data: datos.lstOperaciones,
        columns: [
            {
                data: 'codigo',
                width: '10%',
                className: 'text-center hidden'
            },
            {
                data: 'fecha',
                width: '10%',
                className: 'text-center'
            },
            {
            data: 'documento',
                width: '15%',
                className: 'text-center'
            },
            {
                data: 'nombreCliente',
                width: '45%',
                className: 'text-left'
            },
            {
                data: 'nroDocCliente',
                width: '10%',
                className: 'text-center'
            },
            {
                data: 'ctipo',
                width: '10%',
                className: 'text-center'
            },
            {
                data: 'monto',
                width: '10%',
                className: 'text-center'
            },
            {
                data: 'ver',
                width: '5%',
                className: 'text-center',
                orderable: false
            },
            {
                data: 'reemitir',
                width: '5%',
                className: 'text-center',
                orderable: false
            }
        ],
        order: [[0, 'desc']],
        //scrollY: 400,
        pageResize: true,
        destroy: true,
        searching: false,
        info: false,
        dom: 'rtlp',
        language: {
            url: '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'
        }
    });
}

function fxFiltrarOperaciones(){
    $("#btnBuscar").attr('disabled', 'true');
    
    var nroDocCliente = $("#inpDocCliente").val();
    var nroDocumento = $("#inpNroDocumento").val();
    var fechaIni =  $("#inpFecIni").val();
    var fechaFin =  $("#inpFecFin").val();
    var monto = $("#inpMonto").val();
    if(monto === ''){
        monto = -1;
    }
    
    var parametros = {
        nroDocCliente: nroDocCliente,
        documento: nroDocumento,
        fechaIni: fechaIni,
        fechaFin: fechaFin,
        monto: monto
    };
    
    $.ajax({
        type: 'POST',
        url: 'filtrarOperaciones.action',
        data: parametros,
        success: function (data) {
            if (data.err.length === 0) {
                generarTablaOperaciones(data);
            } else {
                toastr.error(data.err);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            alert("Error : " + textStatus);
        }
    }).done(function(){
        $("#btnBuscar").removeAttr('disabled');
    });
}


//===============================================================================================
//
//                            funnciones flujo de mantenimiento
//                            
//==============================================================================================

function validatePhone(txtPhone) {
    //Si no ingresó número de telefono, no verificarlo
    if(txtPhone === ''){
        return true;
    }
    else{
        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        if (filter.test(txtPhone)) {
            return true;
        }
        else {
            return false;
        }
    }
}

function validarDatos(){
    var validado = true;
    //validar telefonos
    if(!validatePhone($("#formFRO input[id='telefono']").val())){
        validado = false;
        toastr.warning('Ingrese un teléfono válido en FRO');
        $("#formFRO input[id='telefono']").addClass('has-error');
    }else{
        $("#formFRO input[id='telefono']").removeClass('has-error');
    }
        
    if(!validatePhone($("#formCNRO input[id='telefono']").val())){
        validado = false;
        toastr.warning('Ingrese un teléfono válido en CNRO');
        $("#formCNRO input[id='telefono']").addClass('has-error');
    }else{
        $("#formCNRO input[id='telefono']").removeClass('has-error');
    }
    
    if(!validatePhone($("#formFD input[id='telefono']").val())){
        validado = false;
        toastr.warning('Ingrese un teléfono válido en FD');
        $("#formFD input[id='telefono']").addClass('has-error');
    }else{
        $("#formFD input[id='telefono']").removeClass('has-error');
    }
    
    //validar que se haya ingresado un monto
    if($("#formOperacion input[id='monto']").val() === ""){
        validado = false;
        toastr.warning('Ingrese un monto');
        $("#formOperacion input[id='monto']").addClass('has-error');
    }else{
        $("#formOperacion input[id='monto']").removeClass('has-error');
    }
    
    return validado;
}

function flujo_cancelar(){
    borrarInputs();
    deshabilitarInputs();
    cambiarPestania("lst");//cambiar pestania a listado
    $("#btnGrabar").show();
}

function flujo_nuevo(){
    borrarInputs();
    habilitarInputs();
            
    //iniciar con la pestania de FRO en la seccion de mantenimiento
    cambiarPestaniaTipo("FRO");
    //cambiar a pestania mantenimiento
    cambiarPestania("mnt");
    
    //iniciar con la fecha actual
    $('#fecha').val(fechaActual());
    //iniciar con la hora actual
    $('#hora').val(horaActual());
    
    $(".btnGrabar").removeAttr('disabled');
}



function fxGrabarOperacion(datos){
    //var envio = JSON.stringify(datos);
    $.ajax({
        url: 'grabarOperacion.action',
        data: datos,
        type: "POST",
        cache : false,
        async: false,
        dataType: "json",
        success: function(data) { 
            if (data.err.length === 0) {
                toastr.options.timeOut = 3000; //timeout para controlar el tiempo
                toastr.success(data.mensaj);
                fxFiltrarOperaciones();
                flujo_cancelar();//para volver al listado
            } else {
                console.log(data.err);
                toastr.error(data.err);
            }
        },
        error: function(jqXmlHttpRequest, textStatus, errorThrown) { toastr.error("Error: " + errorThrown); }
    }).done(function () {});
}

function flujo_grabar(){
    if(validarDatos()){
        //DATOS DE LA OPERACIÓN    
        var fecha = $("#formOperacion input[id='fecha']").val();
        var hora = $("#formOperacion input[id='hora']").val();
        var documento = $("#formOperacion input[id='documento']").val();
        var tipo = $("#formOperacion select[id='tipo']").val();
        var oficina = $("#formOperacion select[id='oficina']").val();
        var tipoDocCliente = $("#formOperacion select[id='tipoDocCliente']").val();
        var nroDocCliente = $("#formOperacion input[id='nroDocCliente']").val();
        var nombreCliente = $("#formOperacion input[id='cliente']").val();
        var cuenta = $("#formOperacion input[id='cuenta']").val();
        var monto = $("#formOperacion input[id='monto']").val();

        //DETALLES DE LA OPERACIÓN    
        var tipoFondos = $("#formDetOperacion select[id='tipoFondos']").val();
        var tipoOperacion = $("#formDetOperacion select[id='tipoOperacion']").val();
        var descTipoOperacion = $("#formDetOperacion input[id='descTipoOperacion']").val();
        var origenFondos = $("#formDetOperacion textarea[id='origenFondos']").val();
        var moneda = $("#formDetOperacion select[id='moneda']").val();
        var alcanceOperacion = $("#formDetOperacion select[id='alcanceOperacion']").val();
        var codPaisOrigen = $("#formDetOperacion select[id='codPaisOrigen']").val();
        var codPaisDestino = $("#formDetOperacion select[id='codPaisDestino']").val();
        var intermediarioOperacion = $("#formDetOperacion select[id='intermediarioOperacion']").val();
        var formaOperacion = $("#formDetOperacion select[id='formaOperacion']").val();
        var descFormaOperacion = $("#formDetOperacion input[id='descFormaOperacion']").val();
        var origenEntidadInvolucrada = $("#formDetOperacion input[id='origenEntidadInvolucrada']").val();
        var origenTipoCuentaInvolucrada = $("#formDetOperacion select[id='origenTipoCuentaInvolucrada']").val();
        var origenCodCuenta = $("#formDetOperacion input[id='origenCodCuenta']").val();
        var destinoEntidadInvolucrada = $("#formDetOperacion input[id='destinoEntidadInvolucrada']").val();
        var destinoTipoCuentaInvolucrada = $("#formDetOperacion select[id='destinoTipoCuentaInvolucrada']").val();
        var destinoCodCuenta = $("#formDetOperacion input[id='destinoCodCuenta']").val();        
        
        //FRO - FISICAMENTE REALIZA LA OPERACION        
        var fro_tipoRelacionSO = $("#formFRO select[id='tipoRelacionSO']").val();
        var fro_condResidencia = $("#formFRO select[id='condResidencia']").val();
        var fro_tipoPersona = $("#formFRO select[id='tipoPersona']").val();
        var fro_tipoDocumento = $("#formFRO select[id='tipoDocumento']").val();
        var fro_numeroDocumento = $("#formFRO input[id='numeroDocumento']").val();
        var fro_numeroRuc = $("#formFRO input[id='numeroRuc']").val();
        var fro_apellidoPaterno = $("#formFRO input[id='apellidoPaterno']").val();
        var fro_apellidoMaterno = $("#formFRO input[id='apellidoMaterno']").val();
        var fro_nombres = $("#formFRO input[id='nombres']").val();
        var fro_ocupacion = $("#formFRO input[id='ocupacion']").val();
        var fro_descOcupacion = $("#formFRO input[id='descOcupacion']").val();
        var fro_CIIU = $("#formFRO input[id='CIIU']").val();
        var fro_cargo = $("#formFRO input[id='cargo']").val();
        var fro_telefono = $("#formFRO input[id='telefono']").val();
        var fro_nombreVia = $("#formFRO input[id='nombreVia']").val();
        var fro_departamento = $("#formFRO select[id='departamento']").val();
        var fro_provincia = $("#formFRO select[id='provincia']").val();
        var fro_distrito = $("#formFRO select[id='distrito']").val();

        //CNRO - EN CUYO NOMBRE SE REALIZA LA OPERACION           
        var cnro_tipoRelacionSO = $("#formCNRO select[id='tipoRelacionSO']").val();
        var cnro_condResidencia = $("#formCNRO select[id='condResidencia']").val();
        var cnro_tipoPersona = $("#formCNRO select[id='tipoPersona']").val();
        var cnro_tipoDocumento = $("#formCNRO select[id='tipoDocumento']").val();
        var cnro_numeroDocumento = $("#formCNRO input[id='numeroDocumento']").val();
        var cnro_numeroRuc = $("#formCNRO input[id='numeroRuc']").val();
        var cnro_apellidoPaterno = $("#formCNRO input[id='apellidoPaterno']").val();
        var cnro_apellidoMaterno = $("#formCNRO input[id='apellidoMaterno']").val();
        var cnro_nombres = $("#formCNRO input[id='nombres']").val();
        var cnro_ocupacion = $("#formCNRO input[id='ocupacion']").val();
        var cnro_descOcupacion = $("#formCNRO input[id='descOcupacion']").val();
        var cnro_CIIU = $("#formCNRO input[id='CIIU']").val();
        var cnro_cargo = $("#formCNRO input[id='cargo']").val();
        var cnro_telefono = $("#formCNRO input[id='telefono']").val();
        var cnro_nombreVia = $("#formCNRO input[id='nombreVia']").val();
        var cnro_departamento = $("#formCNRO select[id='departamento']").val();
        var cnro_provincia = $("#formCNRO select[id='provincia']").val();
        var cnro_distrito = $("#formCNRO select[id='distrito']").val();
        
        //FD - A FAVOR DE   
        var fd_tipoRelacionSO = $("#formFD select[id='tipoRelacionSO']").val();
        var fd_condResidencia = $("#formFD select[id='condResidencia']").val();
        var fd_tipoPersona = $("#formFD select[id='tipoPersona']").val();
        var fd_tipoDocumento = $("#formFD select[id='tipoDocumento']").val();
        var fd_numeroDocumento = $("#formFD input[id='numeroDocumento']").val();
        var fd_numeroRuc = $("#formFD input[id='numeroRuc']").val();
        var fd_apellidoPaterno = $("#formFD input[id='apellidoPaterno']").val();
        var fd_apellidoMaterno = $("#formFD input[id='apellidoMaterno']").val();
        var fd_nombres = $("#formFD input[id='nombres']").val();
        var fd_ocupacion = $("#formFD input[id='ocupacion']").val();
        var fd_descOcupacion = $("#formFD input[id='descOcupacion']").val();
        var fd_CIIU = $("#formFD input[id='CIIU']").val();
        var fd_cargo = $("#formFD input[id='cargo']").val();
        var fd_telefono = $("#formFD input[id='telefono']").val();
        var fd_nombreVia = $("#formFD input[id='nombreVia']").val();
        var fd_departamento = $("#formFD select[id='departamento']").val();
        var fd_provincia = $("#formFD select[id='provincia']").val();
        var fd_distrito = $("#formFD select[id='distrito']").val();
        
        
        var datos = {
                //datos operacion
                fecha: fecha,
                hora: hora,
                documento: documento,
                tipo: tipo,
                oficina: oficina,
                tipoDocCliente: tipoDocCliente,
                nroDocCliente: nroDocCliente,
                nombreCliente: nombreCliente,
                cuenta: cuenta,
                monto: monto,
                //detalle operacion
                tipoFondos: tipoFondos,
                tipoOperacion: tipoOperacion,
                descTipoOperacion: descTipoOperacion,
                origenFondos: origenFondos,
                moneda: moneda,
                alcanceOperacion: alcanceOperacion,
                codPaisOrigen: codPaisOrigen,
                codPaisDestino: codPaisDestino,
                intermediarioOperacion: intermediarioOperacion,
                formaOperacion: formaOperacion,
                descFormaOperacion: descFormaOperacion,
                origenEntidadInvolucrada: origenEntidadInvolucrada,
                origenTipoCuentaInvolucrada: origenTipoCuentaInvolucrada,
                origenCodCuenta: origenCodCuenta,
                destinoEntidadInvolucrada: destinoEntidadInvolucrada,
                destinoTipoCuentaInvolucrada: destinoTipoCuentaInvolucrada,
                destinoCodCuenta: destinoCodCuenta,
                //datos personas FRO
                fro_tipoRelacionSO: fro_tipoRelacionSO,
                fro_condResidencia: fro_condResidencia,
                fro_tipoPersona: fro_tipoPersona,
                fro_tipoDocumento: fro_tipoDocumento,
                fro_numeroDocumento: fro_numeroDocumento,
                fro_numeroRuc: fro_numeroRuc,
                fro_apellidoPaterno: fro_apellidoPaterno,
                fro_apellidoMaterno: fro_apellidoMaterno,
                fro_nombres: fro_nombres,
                fro_ocupacion: fro_ocupacion,
                fro_descOcupacion: fro_descOcupacion,
                fro_ciiu: fro_CIIU,
                fro_cargo: fro_cargo,
                fro_telefono: fro_telefono,
                fro_nombreVia: fro_nombreVia,
                fro_departamento: fro_departamento,
                fro_provincia: fro_provincia,
                fro_distrito: fro_distrito,
                //CNRO
                cnro_tipoRelacionSO: cnro_tipoRelacionSO,
                cnro_condResidencia: cnro_condResidencia,
                cnro_tipoPersona: cnro_tipoPersona,
                cnro_tipoDocumento: cnro_tipoDocumento,
                cnro_numeroDocumento: cnro_numeroDocumento,
                cnro_numeroRuc: cnro_numeroRuc,
                cnro_apellidoPaterno: cnro_apellidoPaterno,
                cnro_apellidoMaterno: cnro_apellidoMaterno,
                cnro_nombres: cnro_nombres,
                cnro_ocupacion: cnro_ocupacion,
                cnro_descOcupacion: cnro_descOcupacion,
                cnro_ciiu: cnro_CIIU,
                cnro_cargo: cnro_cargo,
                cnro_telefono: cnro_telefono,
                cnro_nombreVia: cnro_nombreVia,
                cnro_departamento: cnro_departamento,
                cnro_provincia: cnro_provincia,
                cnro_distrito: cnro_distrito,
                //FD
                fd_tipoRelacionSO: fd_tipoRelacionSO,
                fd_condResidencia: fd_condResidencia,
                fd_tipoPersona: fd_tipoPersona,
                fd_tipoDocumento: fd_tipoDocumento,
                fd_numeroDocumento: fd_numeroDocumento,
                fd_numeroRuc: fd_numeroRuc,
                fd_apellidoPaterno: fd_apellidoPaterno,
                fd_apellidoMaterno: fd_apellidoMaterno,
                fd_nombres: fd_nombres,
                fd_ocupacion: fd_ocupacion,
                fd_descOcupacion: fd_descOcupacion,
                fd_ciiu: fd_CIIU,
                fd_cargo: fd_cargo,
                fd_telefono: fd_telefono,
                fd_nombreVia: fd_nombreVia,
                fd_departamento: fd_departamento,
                fd_provincia: fd_provincia,
                fd_distrito: fd_distrito
        };

        console.log(datos);
        fxGrabarOperacion(datos);
    }
}




//--funciones mnt Operaciones
function borrarInputs(){
    $("#formOperacion :input").each(function(){
        $(this).val('');
    });
    $("#formDetOperacion :input").each(function(){
        $(this).val('');
    });
    $("#formFRO :input").each(function(){
        $(this).val('');
    });
    $("#formCNRO :input").each(function(){
        $(this).val('');
    });
    $("#formFD :input").each(function(){
        $(this).val('');
    });
    
    //setear cada select en -1
    $(".select").each(function(){
        $(this).val('-1');
    });
    
    //remover el atributo de seleccionado de los checkbox
    $(".form-check-input").each(function(){
        $(this).prop('checked', false); 
    });
    
    //quitar class has-error
    $(".has-error").each(function(){
        $(this).removeClass('has-error');
    });
}

function habilitarInputs(){
    $("#formOperacion :input").each(function(){
       $(this).removeAttr('disabled');
    });
    $("#formDetOperacion :input").each(function(){
        $(this).removeAttr('disabled');
    });
    $("#formFRO :input").each(function(){
        $(this).removeAttr('disabled');
    });
    $("#formCNRO :input").each(function(){
        $(this).removeAttr('disabled');
    });
    $("#formFD :input").each(function(){
        $(this).removeAttr('disabled');
    });
}

function deshabilitarInputs(){
    $("#formOperacion :input").each(function(){
        $(this).attr('disabled', 'true');
    });
    $("#formDetOperacion :input").each(function(){
        $(this).attr('disabled', 'true');
    });
    $("#formFRO :input").each(function(){
        $(this).attr('disabled', 'true');
    });
    $("#formCNRO :input").each(function(){
        $(this).attr('disabled', 'true');
    });
    $("#formFD :input").each(function(){
        $(this).attr('disabled', 'true');
    });
   
}



//=====================================FUNCIONES AUXILIARES==============================

//SCRIPT COMUNES PARA TODOS LOS ARCHIVOS
function fechaActual() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1;
    var yyyy = hoy.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var fechaActual = yyyy + '-' + mm + '-' + dd;
    return fechaActual;
}

function horaActual() {
    var d = new Date();
    var hours = d.getHours();
    var minutes = d.getMinutes();
    if (hours < 10) {
        hours = '0' + hours;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    var horaActual = hours + ':' + minutes;
    return horaActual;
}

function fxReemitir(codigo_){
   $('#codigo').val(codigo_);
   document.formPDF.submit();     
}