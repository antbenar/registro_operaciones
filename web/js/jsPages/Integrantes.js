function inicio(){
    $('#ModalDetCred').on('hidden.bs.modal', function (e) {
        $('body').addClass('modal-open');
         $('.modal').not(this).removeClass('modal-backdrop');
    });

    $('#ModalDetCred').on('show.bs.modal', function() {  
        $('.modal').not(this).addClass('modal-backdrop');
    });
}