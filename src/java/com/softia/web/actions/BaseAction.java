/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.softia.web.actions;

import com.opensymphony.xwork2.ActionSupport;

/**
 *
 * @author Anthony
 */
public class BaseAction extends ActionSupport {
    protected String error = "";
    private String mensaje;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    
}
